import json
from collections import OrderedDict

balance = 100
my_movies_file = open('my_movies.json')
my_movies = OrderedDict()
my_movies = json.load(my_movies_file)

# adjust available cost based on already purchased movies
for genre in my_movies:
    for m in my_movies[genre]:
        balance -= m['cost']
if balance < 0: balance = 0


def menu():
    print("1. List all available movies")
    print("2. Show my movies")
    print("3. Purchase a movie")
    print("4. Show credit balance")
    print("5. Show Genres")
    print("6. Search a Movie")
    choice = input('Enter your choice [1-6] : ')

    if choice == '1':
        list_all()
        go_to_main()
    if choice == '2':
        show_my_movies()
        go_to_main()
    if choice == '3':
        purchase()
        go_to_main()
    if choice == '4':
        show_balance(cost=0)
        go_to_main()
    if choice == '5':
        show_genres()
        go_to_main()
    if choice == '6':
        search_movies()
        go_to_main()


def go_to_main():
    main = input("Do you want to go back to Main Menu ? (y/n)")
    if main == 'y':
        menu()
    else:
        exit()


def list_all():  # List all available movies
    json_file = open('available_movies.json')
    json_data = json.load(json_file)
    for key, values in json_data.items():
        print("GENRE : " + key.upper())
        for value in values:
            print("Title: " + '"' + value['title'] + '"' + "," + " Rating: " + str(value['rating']) + "," + " Cost: " + str(value['cost']))


def show_my_movies():  # My Movies List
    print("My Movies List")
    for key, values in my_movies.items():
        for value in values:
            print("Title: " + '"' + value['title'] +'"' + "," + " Rating: " + str(value['rating']))
            if value['watched'] is False:
                print("Watched: NO")
            else:
                print("Watched: YES")


def purchase():
    movie = input("Enter the movie you want to purchase :")
    json_file = open('available_movies.json')
    movies_list = json.load(json_file)
    flag = False
    for key, values in movies_list.items():
        for value in values:
            if movie == value['title']:  # for exact word search
                flag = True
                for genre in my_movies:
                    matched = [m for m in my_movies[genre] if m['title'] == movie]
                    if matched:
                        break
                if matched:
                    print("You have already purchased this movie")
                else:
                    purchase_movie(movie)

    if not flag:
        print("We currently don't have this movie")


def purchase_movie(movie):
    json_file = open('available_movies.json')
    movies_list = json.load(json_file)
    for key, values in movies_list.items():
        for value in values:
            if movie == value['title']:
                cost = value['cost']
                if balance >= cost:
                    print("The cost is " + str(cost))
                    proceed = input("Do you want to proceed ? (y/n)")
                    if proceed == 'y':
                        show_balance(cost)
                        b=value
                        b.update({'watched': 'false'})
                        a = []
                        a.append(dict(b))
                        new_dict = {}
                        new_dict[key] = a
                        my_movies.update(new_dict)
                        with open('my_movies.json', 'w') as outfile:
                            json.dump(my_movies, outfile)
                else:
                    print("Not enough credit remaining to purchase this movie")


def show_balance(cost):  # Show credit balance
    global balance
    current_balance = balance - cost
    balance = current_balance
    print("Your credit balance is ", current_balance)


def show_genres():  # Show Genre and movies in the genre
    json_file = open('available_movies.json')
    movies_list = json.load(json_file)
    for genres in movies_list:
        print(genres)
    genre_select = input("Select a Genre :")
    data = movies_list[genre_select]
    for t in data:
        print(t['title'])


def search_movies():  # Search for a specif movie and purchase
    keyword = input("Enter movie to search")
    json_file = open('available_movies.json')
    movies_list = json.load(json_file)
    match_flag = False
    for key, values in movies_list.items():
        for value in values:
            if keyword in value['title']:
                print(value['title'])
                match_flag = True
    if not match_flag:
        print("No movies found matching", keyword)
    if match_flag:
        buy = input("Do you want to purchase ? (y/n)")
        if buy == 'y':
            purchase()


if __name__ == "__main__":
    menu()


